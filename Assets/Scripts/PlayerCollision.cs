﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public GameObject hurtMessage;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag =="Table" || collision.collider.tag == "Vase")
        {
            collision.gameObject.GetComponent<AudioSource>().Play();
            hurtMessage.SetActive(true);
            Invoke("offHurt", 0.5f);
        }
    }

    void offHurt()
    {
        hurtMessage.SetActive(false);
    }
}
