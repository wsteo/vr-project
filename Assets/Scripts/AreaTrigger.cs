﻿using UnityEngine;

public class AreaTrigger : MonoBehaviour
{
    public GameObject ShowText;
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject InteractText;
    float CoordinateDistance;

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            InteractText.SetActive(true);
            if(Input.GetKeyDown("e"))
            {
                ShowText.SetActive(true); 
            }
            
        }
        else
        {
            InteractText.SetActive(false);
            ShowText.SetActive(false);
        }
    }

}
