﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoveInteraction : MonoBehaviour
{
    public GameObject ShowText;
    public GameObject ShowCookText;
    public GameObject GasOff;

    public GameObject MainCharacter;
    public GameObject fish;
    public GameObject TargetArea;
    public GameObject InteractText;
    public GameObject CookText;
    public GameObject resetText;
    bool reset =true;
    bool cook = false;
    float CoordinateDistance;

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            if (reset){
                resetText.SetActive(false);
                if (!cook) 
                {
                    InteractText.SetActive(true);
                    CookText.SetActive(false);
                }
                else {
                    InteractText.SetActive(false);
                    CookText.SetActive(true);
                }
            
            if (Input.GetKeyDown("e"))
            {
                GasOff.SetActive(false);
                ShowCookText.SetActive(false);
                ShowText.SetActive(true);
                cook = true;
            }
            if (Input.GetKeyDown("r"))
            {
                CookText.SetActive(false);
                ShowText.SetActive(false);
                ShowCookText.SetActive(true);
                fish.SetActive(true);
                reset=!reset;
            }
            }else
            {
                resetText.SetActive(true);
                if(Input.GetKeyDown("e"))
                {
                    ShowCookText.SetActive(false);
                    fish.SetActive(false);
                    GasOff.SetActive(true);
                    reset = !reset;
                    cook = false;
                }
            }


        }
        else
        {
            InteractText.SetActive(false);
            ShowText.SetActive(false);
            GasOff.SetActive(false);
            ShowCookText.SetActive(false);
        }
    }
}
