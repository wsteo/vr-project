﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    public GameObject Lights;
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject OnText;
    public GameObject OffText;
    public GameObject Sound;
    float CoordinateDistance;
    bool onoff=true;

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            if(onoff){
                OffText.SetActive(true);
                OnText.SetActive(false);
            if (Input.GetKeyDown("e"))
            {
                Lights.SetActive(false);
                Sound.GetComponent<AudioSource>().Play();
                 onoff = !onoff;
            }
            }else
            {
                OffText.SetActive(false);
                OnText.SetActive(true);
                if (Input.GetKeyDown("e"))
                {
                    Lights.SetActive(true);
                    Sound.GetComponent<AudioSource>().Play();
                    onoff = !onoff;
                }

            }

        }
        else
        {
            OnText.SetActive(false);
            OffText.SetActive(false);
        }
    }
}
