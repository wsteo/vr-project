﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speech : MonoBehaviour
{
    public GameObject ShowText;
    public GameObject ShowText2;
    public GameObject ShowText3;
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject InteractText;
    public GameObject character;
    public GameObject character2;
    public GameObject food;
    float CoordinateDistance;

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            InteractText.SetActive(true);
            if (food.activeSelf) {
                if (Input.GetKeyDown("e"))
                {
                    ShowText3.SetActive(false);
                    ShowText2.SetActive(true);
                    MainCharacter.GetComponent<AudioSource>().Stop();
                    character2.GetComponent<AudioSource>().Play();
                }
                if (Input.GetKeyDown("r"))
                {
                    ShowText2.SetActive(false);
                    ShowText3.SetActive(true);
                    character2.GetComponent<AudioSource>().Stop();
                    MainCharacter.GetComponent<AudioSource>().Play();
                }
            }
            else { 
            if (Input.GetKeyDown("e"))
            {
                ShowText.SetActive(true);
                character.GetComponent<AudioSource>().Play();
                character2.GetComponent<AudioSource>().Stop();
                MainCharacter.GetComponent<AudioSource>().Stop();
                }
            }

        }
        else
        {
            InteractText.SetActive(false);
            ShowText.SetActive(false);
            ShowText2.SetActive(false);
            ShowText3.SetActive(false);
        }
    }
}
