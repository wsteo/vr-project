﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransfer : MonoBehaviour
{
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject InteractText;
    float CoordinateDistance;

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            InteractText.SetActive(true);
            if (Input.GetKeyDown("e"))
            {
                SceneManager.LoadScene("SampleScene");
            }

        }
        else
        {
            InteractText.SetActive(false);
        }
    }
}
