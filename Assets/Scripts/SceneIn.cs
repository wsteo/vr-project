﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneIn : MonoBehaviour
{
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject InteractText;
    float CoordinateDistance;

    public string[] name = {"MLTesting","WS Kitchen","Liang Kitchen","Clanie","Lokman"}; 

    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            InteractText.SetActive(true);
            if (Input.GetKeyDown("e"))
            {
                if (TargetArea.tag == "Room1")
                {
                    SceneManager.LoadScene(name[0]);
                }
                else if(TargetArea.tag== "Room2")
                {
                    SceneManager.LoadScene(name[1]);
                }
                else if (TargetArea.tag == "Room3")
                {
                    SceneManager.LoadScene(name[2]);
                }
                else if (TargetArea.tag == "Room4")
                {
                    SceneManager.LoadScene(name[3]);
                }
                else if (TargetArea.tag == "Room5")
                {
                    SceneManager.LoadScene(name[4]);
                }
            }

        }
        else
        {
            InteractText.SetActive(false);
        }
    }
}
