﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlay : MonoBehaviour
{
    public GameObject Video;
    public GameObject MainCharacter;
    public GameObject TargetArea;
    public GameObject InteractText;
    float CoordinateDistance;
    bool isVideo=false;
    private void Update()
    {
        CoordinateDistance = Vector3.Distance(MainCharacter.transform.position, TargetArea.transform.position);
        if (CoordinateDistance < 2)
        {
            if (!isVideo)
            {
                InteractText.SetActive(true);
                if (Input.GetKeyDown("e"))
                {
                    Video.SetActive(true);
                    isVideo = !isVideo;
                }
            }else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Video.SetActive(false);
                    isVideo = !isVideo; 
                }
            }

        }
        else
        {
            InteractText.SetActive(false);
            Video.SetActive(false);
            isVideo = false;
        }
    }
}
